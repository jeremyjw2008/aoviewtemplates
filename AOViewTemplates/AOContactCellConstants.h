//
//  AOContactCellConstants.h
//  AOViewTemplates
//
//  Created by Anthony Miller on 7/15/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

extern NSString * const AOContactCellPhoneNumberKey;

extern NSString * const AOContactCellEmailKey;