//
//  AOTemplateCellIdentifierConstants.h
//  AOViewTemplates
//
//  Created by Anthony Miller on 7/14/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

extern NSString * const AOContactCellIdentifier;

extern NSString * const AOMenuCellIdentifier;

extern NSString * const AOHeaderImageCellIdentifier;

extern NSString * const AOHeadingCellIdentifier;