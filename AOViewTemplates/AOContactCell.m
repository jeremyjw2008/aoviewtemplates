//
//  AOContactCell.m
//  AOViewTemplates
//
//  Created by Anthony Miller on 7/15/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import "AOContactCell.h"

#import "AOTemplateCellConstants.h"
#import "AOContactCellConstants.h"

@implementation AOContactCell

- (void)setValuesDictionary:(NSDictionary *)valuesDictionary
{
  [super setValuesDictionary:valuesDictionary];
  
  [self setPhoneNumberFromDictionary:valuesDictionary];
  [self setEmailAddressFromDictionary:valuesDictionary];
}

#pragma mark - Set Phone Number

- (void)setPhoneNumberFromDictionary:(NSDictionary *)dictionary
{
  if (dictionary[AOContactCellPhoneNumberKey]) {
    [self setPhoneNumberString:dictionary[AOContactCellPhoneNumberKey]];
    
  } else {
    [self setPhoneNumberTextViewConstraintsToZero];
  }
}

- (void)setPhoneNumberString:(NSString *)phoneNumberString
{
  self.phoneNumberTextView.text = phoneNumberString;
}

- (void)setPhoneNumberTextViewConstraintsToZero
{
  self.subtitleLabelToPhoneNumberSpaceConstraint.constant = 0;
  self.phoneNumberHeightConstraint.constant = 0;
}

#pragma mark - Set E-mail Address

- (void)setEmailAddressFromDictionary:(NSDictionary *)dictionary
{
  if (dictionary[AOContactCellEmailKey]) {
    [self setEmailAddressString:dictionary[AOContactCellEmailKey]];
  } else {
    [self setEmailTextViewConstraintsToZero];
  }
}

- (void)setEmailAddressString:(NSString *)emailAddressString
{
  self.emailTextView.text = emailAddressString;
}

- (void)setEmailTextViewConstraintsToZero
{
  self.phoneNumberToEmailSpaceConstraint.constant = 0;
  self.emailHeightConstraint.constant = 0;
}

#pragma mark - Set Font Color

- (void)setFontColor:(UIColor *)fontColor
{
  [super setFontColor:fontColor];
  
  self.phoneNumberTextView.textColor = fontColor;
  self.emailTextView.textColor = fontColor;
}

@end
