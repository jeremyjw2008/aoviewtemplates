//
//  NSBundle+AOViewTemplatesBundle.h
//  AOViewTemplates
//
//  Created by Anthony Miller on 7/11/14.
//  Copyright (c) 2014 App-Order, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  This is the name of the `AOViewTemplatesBundle`.
 *
 *  @note If you change this value, you *must* bump the **major** version of the library (as such a change is **not** backwards compatible)! Do not change this value without good reason!
 */
extern NSString * const AOViewTemplatesBundleName;

/**
 *  This category provides a convenience method for getting the `AOViewTemplatesBundle`, which includes all of the nibs and assets for the `AOViewTemplates` library.
 */
@interface NSBundle (AOViewTemplatesBundle)

/**
 *  This is a convenience method for getting a reference to the `AOViewTemplatesBundle` shared bundle.
 *
 *  @discussion This method sets `AOViewTemplatesBundle = [self bundleWithAOViewTemplatesBundlePath]`.
 *
 *
 *  @return A reference to the `AOViewTemplates` bundle.
 */
+ (NSBundle *)AOViewTemplatesBundle;

/**
 *  This method creates a new `NSBundle` with its path set using `AOViewTemplatesBundleName`.
 *
 *  @warning This method is exposed for unit testing. In general, you should use `AOViewTemplatesBundle` instead.
 *
 *  @return A new instance of `NSBundle`
 */
+ (instancetype)AOCreateViewTemplatesBundle;
@end

