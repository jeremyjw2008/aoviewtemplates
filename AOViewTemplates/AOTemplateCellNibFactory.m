//
//  AOTemplateCellNibFactory.m
//  AOViewTemplates
//
//  Created by Anthony Miller on 7/14/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import "AOTemplateCellNibFactory.h"

#import "NSBundle+AOViewTemplatesBundle.h"

@implementation AOTemplateCellNibFactory

+ (UINib *)menuCellNib
{
  return [self nibWithName:@"AOMenuCell"];
}

+ (UINib *)headerImageCellNib
{
  return [self nibWithName:@"AOHeaderImageCell"];
}

+ (UINib *)headingCellNib
{
  return [self nibWithName:@"AOHeadingCell"];
}

+ (UINib *)contactCellNib
{
  return [self nibWithName:@"AOContactCell"];
}

+ (UINib *)nibWithName:(NSString *)name
{
  UINib *nib = [UINib nibWithNibName:name bundle:[NSBundle AOViewTemplatesBundle]];
  return nib;
}

@end
