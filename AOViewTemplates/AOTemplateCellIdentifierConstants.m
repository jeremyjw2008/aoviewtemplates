//
//  AOTemplateCellIdentifierConstants.m
//  AOViewTemplates
//
//  Created by Anthony Miller on 7/14/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

NSString * const AOContactCellIdentifier = @"AOContactCellIdentifier";

NSString * const AOMenuCellIdentifier = @"AOMenuCellIdentifier";

NSString * const AOHeaderImageCellIdentifier = @"AOHeaderImageCellIdentifier";

NSString * const AOHeadingCellIdentifier = @"AOHeadingCellIdentifier";