//
//  NSBundle+AOReportControllersBundle.m
//  AOReportControllers
//
//  Created by Joshua Greene on 6/4/14.
//  Copyright (c) 2014 App-Order, LLC. All rights reserved.
//

#import "NSBundle+AOViewTemplatesBundle.h"

NSString * const AOViewTemplatesBundleName = @"AOViewTemplates.bundle";

@implementation NSBundle (AOViewTemplatesBundle)

+ (NSBundle *)AOViewTemplatesBundle
{
  static dispatch_once_t onceToken;
  static NSBundle *AOViewTemplatesBundle = nil;
  
  dispatch_once(&onceToken, ^{
    AOViewTemplatesBundle = [self AOCreateViewTemplatesBundle];
  });
  
  return AOViewTemplatesBundle;
}

+ (instancetype)AOCreateViewTemplatesBundle
{
  NSString *mainBundlePath = [[NSBundle mainBundle] resourcePath];
  NSString *path = [mainBundlePath stringByAppendingPathComponent:AOViewTemplatesBundleName];
  return [self bundleWithPath:path];
}

@end
