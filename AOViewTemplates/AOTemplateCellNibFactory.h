//
//  AOTemplateCellNibFactory.h
//  AOViewTemplates
//
//  Created by Anthony Miller on 7/14/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

@import UIKit;

@interface AOTemplateCellNibFactory : NSObject

+ (UINib *)menuCellNib;
+ (UINib *)headerImageCellNib;
+ (UINib *)headingCellNib;
+ (UINib *)contactCellNib;

@end
