//
//  AOContactCellConstants.m
//  AOViewTemplates
//
//  Created by Anthony Miller on 7/15/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

NSString * const AOContactCellPhoneNumberKey = @"phoneNumber";

NSString * const AOContactCellEmailKey = @"emailAddress";