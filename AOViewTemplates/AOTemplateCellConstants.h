//
//  AOTemplateCellConstants.h
//  AOViewTemplates
//
//  Created by Anthony Miller on 7/15/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

extern NSString * const AOTemplateCellBackgroundColorKey;

extern NSString * const AOTemplateCellFontColorKey;