//
//  AOBaseTemplateViewController.h
//  AOViewTemplates
//
//  Created by Anthony Miller on 7/11/14.
//  Copyright (c) 2014 App-Order. All rights reserved.


#import "AOBaseTableViewController.h"

#import <AutoLayoutCells/ALTableViewCellFactoryDelegate.h>

@class ALTableViewCellFactory;

@interface AOBaseTemplateViewController : AOBaseTableViewController <ALTableViewCellFactoryDelegate>

- (instancetype)initWithContent:(NSArray *)content;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *contentItems;
@property (strong, nonatomic) ALTableViewCellFactory *cellFactory;

@end