//
//  AOBaseTemplateImageCell.m
//  AOViewTemplates
//
//  Created by Anthony Miller on 7/15/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import "AOBaseTemplateCell.h"

#import <AutoLayoutCells/ALCellConstants.h>
#import <HexColors/HexColor.h>

#import "AOTemplateCellConstants.h"

@implementation AOBaseTemplateCell

- (void)setValuesDictionary:(NSDictionary *)valuesDictionary
{
  [super setValuesDictionary:valuesDictionary];
  
  [self setFontColorFromDictionary:valuesDictionary];
  [self setBackgroundColorFromDictionary:valuesDictionary];
}

#pragma mark - Set Font Color

- (void)setFontColorFromDictionary:(NSDictionary *)dictionary
{
  if (dictionary[AOTemplateCellFontColorKey]) {
    UIColor *fontColor = [UIColor colorWithHexString:dictionary[AOTemplateCellFontColorKey]];
    
    [self setFontColor:fontColor];
  }
}

- (void)setFontColor:(UIColor *)fontColor
{
  self.titleLabel.textColor = fontColor;
  self.subtitleLabel.textColor = fontColor;
}

#pragma mark - Set Background Color

- (void)setBackgroundColorFromDictionary:(NSDictionary *)dictionary
{
  if (dictionary[AOTemplateCellBackgroundColorKey]) {
    [self setBackgroundColor:[UIColor colorWithHexString:dictionary[AOTemplateCellBackgroundColorKey]]];
  }
}

#pragma mark - setTitleFromDictionary:

- (void)setTitleFromDictionary:(NSDictionary *)dictionary
{
  if (dictionary[ALCellAttributedTitleKey]) {
    [self setAttributedTitleString:dictionary[ALCellAttributedTitleKey]];
    
  } else if (dictionary[ALCellTitleKey]) {
    [self setTitleString:dictionary[ALCellTitleKey]];
    
  } else {
    [self setTitleLabelConstraintsToZero];
  }
}

- (void)setTitleLabelConstraintsToZero
{
  self.titleLabelToSubtitleLabelSpaceConstraint.constant = 0;
}

#pragma mark - setSubtitleFromDictionary:

- (void)setSubtitleFromDictionary:(NSDictionary *)dictionary
{
  if (dictionary[ALCellAttributedSubtitleKey]) {
    [self setAttributedSubtitleString:dictionary[ALCellAttributedSubtitleKey]];
    
  } else if (dictionary[ALCellSubtitleKey]) {
    [self setSubtitleString:dictionary[ALCellSubtitleKey]];
    
  } else {
    [self setSubtitleLabelConstraintsToZero];
  }
}

- (void)setSubtitleLabelConstraintsToZero
{
  self.titleLabelToSubtitleLabelSpaceConstraint.constant = 0;
}

@end
