//
//  AOContactCell.h
//  AOViewTemplates
//
//  Created by Anthony Miller on 7/15/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import "AOBaseTemplateCell.h"

@interface AOContactCell : AOBaseTemplateCell

- (void)setPhoneNumberString:(NSString *)phoneNumberString;

- (void)setEmailAddressString:(NSString *)emailAddressString;

@property (unsafe_unretained, nonatomic) IBOutlet UITextView *phoneNumberTextView;
@property (unsafe_unretained, nonatomic) IBOutlet UITextView *emailTextView;


@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *phoneNumberHeightConstraint;
@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *emailHeightConstraint;
@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *subtitleLabelToPhoneNumberSpaceConstraint;
@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *phoneNumberToEmailSpaceConstraint;
@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *emailBottomSpaceConstraint;

@end
