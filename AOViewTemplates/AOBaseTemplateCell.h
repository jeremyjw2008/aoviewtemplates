//
//  AOBaseTemplateImageCell.h
//  AOViewTemplates
//
//  Created by Anthony Miller on 7/15/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import "ALImageCell.h"

@interface AOBaseTemplateCell : ALImageCell

- (void)setFontColor:(UIColor *)fontColor;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelTopSpaceConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelToSubtitleLabelSpaceConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *subtitleLabelBottomSpaceConstraint;


@end
