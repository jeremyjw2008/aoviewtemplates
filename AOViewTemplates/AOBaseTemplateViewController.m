//
//  AOBaseTemplateViewController.m
//  AOViewTemplates
//
//  Created by Anthony Miller on 7/11/14.
//  Copyright (c) 2014 App-Order. All rights reserved.

#import "AOBaseTemplateViewController.h"

#import <AutoLayoutCells/ALCell.h>
#import <AutoLayoutCells/ALTableViewCellFactory.h>
#import <AutoLayoutCells/ALTableViewCellNibFactory.h>
#import <AutoLayoutCells/ALImageCell.h>
#import <UIView+AORefreshFont/UIView+AORefreshFont.h>

#import "AOTemplateCellNibFactory.h"
#import "AOTemplateCellIdentifierConstants.h"
#import "NSBundle+AOViewTemplatesBundle.h"


@interface AOBaseTemplateViewController ()

@end

@implementation AOBaseTemplateViewController

+ (NSBundle *)nibBundleOrNil
{
  return [NSBundle AOViewTemplatesBundle];
}

- (instancetype)initWithContent:(NSArray *)content
{
  self = [super init];
  if (self) {
    self.contentItems = content;
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  [self setupCellFactory];
}

- (void)setupCellFactory
{
  self.cellFactory = [[ALTableViewCellFactory alloc] initWithTableView:self.tableView
                                  identifiersToNibsDictionary:[self cellIdentifierToNibDictionary]];
  self.cellFactory.delegate = self;
}

- (NSDictionary *)cellIdentifierToNibDictionary
{
  UINib *headerImageCellNib = [AOTemplateCellNibFactory headerImageCellNib];
  UINib *headingCellNib = [AOTemplateCellNibFactory headingCellNib];
  UINib *menuCellNib = [AOTemplateCellNibFactory menuCellNib];
  UINib *contactCellNib = [AOTemplateCellNibFactory contactCellNib];
  
  return @{AOHeaderImageCellIdentifier: headerImageCellNib,
           AOHeadingCellIdentifier: headingCellNib,
           AOContactCellIdentifier: contactCellNib,
           AOMenuCellIdentifier: menuCellNib};
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return self.contentItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  UITableViewCell *cell = nil;
  
  NSDictionary *contentItem = self.contentItems[indexPath.row];
  
  NSString *cellIdentifier = [self cellIdentifierForContentItem:contentItem];
  
  cell = [self.cellFactory cellWithIdentifier:cellIdentifier forIndexPath:indexPath];
  
  return cell;
}

- (NSString *)cellIdentifierForContentItem:(NSDictionary *)contentItem
{
  if ([[contentItem objectForKey:@"type"] isEqualToString: @"headerImage"]) {
    return AOHeaderImageCellIdentifier;
    
  } else if ([[contentItem objectForKey:@"type"] isEqualToString: @"heading"]) {
    return AOHeadingCellIdentifier;
    
  } else if ([[contentItem objectForKey:@"type"] isEqualToString:@"contact"]) {
    return AOContactCellIdentifier;
  } else {
    return AOMenuCellIdentifier;
  }
}


#pragma mark - AOCellFactoryDelegate

- (void)configureCell:(id)cell atIndexPath:(NSIndexPath *)indexPath
{
  NSDictionary *contentItem = self.contentItems[indexPath.row];
  
  [cell setValuesDictionary:contentItem];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  NSDictionary *contentItem = self.contentItems[indexPath.row];
  
  NSString *cellIdentifier = [self cellIdentifierForContentItem:contentItem];
  return [self.cellFactory cellHeightForIdentifier:cellIdentifier atIndexPath:indexPath];
}

#pragma mark - Dynamic Type Text

- (void)refreshFonts
{
  [super refreshFonts];
  
  [self.tableView AORefreshFont];
}
@end
