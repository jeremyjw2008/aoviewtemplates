//
//  DemoTableViewController.m
//  AOViewTemplatesDemo
//
//  Created by Anthony Miller on 7/14/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import "DemoTableViewController.h"

#import <AOViewTemplates/NSBundle+AOViewTemplatesBundle.h>

#import <AODataHelper/AODataHelper.h>

@interface DemoTableViewController ()

@end

@implementation DemoTableViewController

+ (NSBundle *)nibBundleOrNil
{
  return [NSBundle AOViewTemplatesBundle];
}

+ (NSString *)nibNameOrNil
{
  return NSStringFromClass([self superclass]);
}

#pragma mark - Toolbar Setup

- (void)setUpToolbarItems:(NSMutableArray *)toolbarItems
{
  [super setUpToolbarItems:toolbarItems];
  [self setUpPageOneButton:toolbarItems];
  [self setUpPageTwoButton:toolbarItems];
}

- (void)setUpPageOneButton:(NSMutableArray *)toolbarItems
{
  UIBarButtonItem *pageOneButton = [[UIBarButtonItem alloc]
                                    initWithBarButtonSystemItem:UIBarButtonSystemItemCamera
                                    target:self
                                    action:@selector(didPressPageOneButton:)];
  
  [toolbarItems addObject:pageOneButton];
}

- (void)setUpPageTwoButton:(NSMutableArray *)toolbarItems
{
  UIBarButtonItem *pageTwoButton = [[UIBarButtonItem alloc]
                                    initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize
                                    target:self
                                    action:@selector(didPressPageTwoButton:)];
  
  [toolbarItems addObject:pageTwoButton];
}

- (void)didPressPageOneButton:(id)sender
{
  NSArray *contentArray = [AODataHelper plistArrayFromFileNamed:@"PageOne"];
  AOBaseTemplateViewController *pageOneVC = [[AOBaseTemplateViewController alloc] initWithContent:contentArray];
  [self.navigationController pushViewController:pageOneVC animated:YES];
}

- (void)didPressPageTwoButton:(id)sender
{
  NSArray *contentArray = [AODataHelper plistArrayFromFileNamed:@"HeaderImagePage"];
  AOBaseTemplateViewController *pageTwoVC = [[AOBaseTemplateViewController alloc] initWithContent:contentArray];
  [self.navigationController pushViewController:pageTwoVC animated:YES];
}

@end
