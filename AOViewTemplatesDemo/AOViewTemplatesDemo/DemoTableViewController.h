//
//  DemoTableViewController.h
//  AOViewTemplatesDemo
//
//  Created by Anthony Miller on 7/14/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import "AOBaseTemplateViewController.h"

@interface DemoTableViewController : AOBaseTemplateViewController

@end
