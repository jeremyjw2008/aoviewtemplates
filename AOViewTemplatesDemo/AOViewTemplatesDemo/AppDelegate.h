//
//  AppDelegate.h
//  AOViewTemplatesDemo
//
//  Created by Anthony Miller on 7/11/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
