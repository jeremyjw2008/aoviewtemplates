Pod::Spec.new do |s|
  s.platform     = :ios
  s.ios.deployment_target = '7.0'
  s.name         = "AOViewTemplates"
  s.version      = "0.0.1"
  s.summary      = ""
  s.homepage     = "http://redmine.app-order.com"
  s.license      = { :type => "Closed Source", :file => "LICENSE.txt" }
  s.author       = { "Anthony Miller" => "anthony@app-order.com" }
  s.source   	 = { :git => "https://bitbucket.org/app-order/aoviewtemplates.git",
                     :tag => "#{s.version}"}
  s.requires_arc = true
    
  s.resource_bundles = {'AOViewTemplates' => ['AOViewTemplates/ResourcesBundle/*/*']}

  s.dependency 'AutoLayoutCells', '~> 0.1'
  s.dependency 'AOBaseControllers', '~> 2.0'
  s.dependency 'AOPhotoPageViewController', '~> 1.2'
  s.dependency 'HexColors', '~> 2.2'
  s.dependency 'UIView+AORefreshFont', '~> 1.0'
  
  s.source_files = "AOViewTemplates/*.{h,m}"
end